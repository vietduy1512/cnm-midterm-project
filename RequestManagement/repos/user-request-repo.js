var db = require('../database/sqlite-db');

exports.loadAll = () => {
	var sql = 'select * from user_requests';
	return db.load(sql);
}

exports.removeRequestById = (id) => {
	var sql = `delete from user_requests where id = ${id}`;
	return db.load(sql);
}

exports.getRequestById = (id) => {
	var sql = `select * from user_requests where id = ${id}`;
	return db.load(sql);
}

exports.getRequestByDriverId = (driverId) => {
	var sql = `select * from user_requests where driver_id = ${driverId}`;
	return db.load(sql);
}

exports.getLocatedRequest = () => {
	var sql = `select * from user_requests where state = 'Located'`;
	return db.load(sql);
}

exports.exec = (sql) => {
	return db.load(sql);
}