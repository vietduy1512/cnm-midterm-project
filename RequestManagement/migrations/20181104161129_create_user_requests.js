
exports.up = function(knex, Promise) {
    return knex.schema.createTable('user_requests', function(t) {
        t.increments('id').primary()
        t.string('name').notNullable()
        t.string('phone_number').notNullable()
        t.string('pickup_address').notNullable()
        t.string('formatted_address')
        t.float('lat')
        t.float('lng')
        t.string('note')
        t.string('driver_name')
        t.integer('driver_id')
        t.float('driver_lat')
        t.float('driver_lng')
        t.string('state')
        t.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists('user_requests')
};