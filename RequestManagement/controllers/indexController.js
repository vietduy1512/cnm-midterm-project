var http = require('http');
var events = require('./eventController');
var userRequestRepo = require('../repos/user-request-repo');


var options = {
    host: 'localhost',
    port: 3003,
    path: '/findDriverForRequest',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    }
};

exports.main = function (req, res) {
    var template = '{{#each this}}<tr>' + 
                '<td style="display:none;" value="{{id}}"></td>' +
                '<td>{{name}}</td><td>{{phone_number}}</td><td>{{pickup_address}}</td><td>{{formatted_address}}</td><td>{{lat}}</td><td>{{lng}}</td><td>{{note}}</td><td>{{driver_name}}</td>{{{printState state}}}<td><button type="button" class="show-distance-btn" onclick="">Show</button></td>' +
                '</tr>{{/each}}';

    userRequestRepo.loadAll()
        .then(rows => {
            res.render('main', { template: template, data_table: rows });
        }).catch(err => {
            res.statusCode = 500;
            res.end('View error log on console');
        });
    
}

exports.receiveNewRequestFromLocationIdentifier = function (req, res) {
    var userRequest = {
        id: req.body.id,
        name: req.body.name,
        phone_number: req.body.phone_number,
        pickup_address: req.body.pickup_address,
        note: req.body.note,
        state: 'Unlocated'
    }

    var query = `insert into user_requests (id, name, phone_number, pickup_address, note, state) values ('${userRequest.id}', '${userRequest.name}', '${userRequest.phone_number}', '${userRequest.pickup_address}', '${userRequest.note}', '${userRequest.state}')`;
    userRequestRepo.exec(query)
        .catch((err) => {
            console.log(err);
        });

    events.publishUserRequestAdded(userRequest);
    res.sendStatus(200);
}

exports.receiveLocatedRequestFromLocationIdentifier = function (req, res) {
    var userRequest = {
        id: req.body.id,
        name: req.body.name,
        phone_number: req.body.phone_number,
        pickup_address: req.body.pickup_address,
        formatted_address: req.body.formatted_address,
        lat: req.body.lat,
        lng: req.body.lng,
        note: req.body.note,
        state: 'Located'
    }

    var query = `update user_requests set formatted_address='${userRequest.formatted_address}', lat='${userRequest.lat}', lng='${userRequest.lng}', state='${userRequest.state}' where id = ${userRequest.id}`;
    userRequestRepo.exec(query)
        .catch((err) => {
            console.log(err);
        });
    events.publishUserRequestUpdated(userRequest);

    // send to Driver
    sendUserRequest(options, userRequest);

    res.sendStatus(200);
}

exports.findDriverForAllLocatedRequest = function(req, res) {
    userRequestRepo.getLocatedRequest()
        .then(requests => {
            for (var request of requests) {
                sendUserRequest(options, request);
            }
        }).catch(err => {
            console.log(err);
        });
    if (res) {
        res.sendStatus(200);
    }
}

exports.receiveTakenRequestFromDriver = function (req, res) {
    var userRequest = {
        id: req.body.id,
        name: req.body.name,
        phone_number: req.body.phone_number,
        pickup_address: req.body.pickup_address,
        formatted_address: req.body.formatted_address,
        lat: req.body.lat,
        lng: req.body.lng,
        note: req.body.note,
        driver_id: req.body.driver_id,
        driver_name: req.body.driver_name,
        driver_lat: req.body.driver_lat,
        driver_lng: req.body.driver_lng,
        state: 'Taken'
    }

    var query = `update user_requests set driver_id='${userRequest.driver_id}', driver_name='${userRequest.driver_name}', driver_lat='${userRequest.driver_lat}', driver_lng='${userRequest.driver_lng}', state='${userRequest.state}' where id = ${userRequest.id}`;
    userRequestRepo.exec(query)
        .catch((err) => {
            console.log(err);
        });
    events.publishUserRequestUpdated(userRequest);

    res.sendStatus(200);
}

exports.updateMovingState = function (req, res) {
    var query = `update user_requests set state='Moving' where driver_id = ${req.body.driver_id}`;
    userRequestRepo.exec(query)
        .then(() => {
            userRequestRepo.getRequestByDriverId(req.body.driver_id)
                .then(requests => {
                    for (var request of requests) {
                        events.publishUserRequestUpdated(request);
                    }
                }).catch(err => {
                    console.log(err);
                });
        }).catch((err) => {
            console.log(err);
        });
    
    res.sendStatus(200);
}

exports.updateFinishState = function (req, res) {
    var query = `update user_requests set state='Finished' where driver_id = ${req.body.driver_id}`;
    userRequestRepo.exec(query)
        .then(() => {
            userRequestRepo.getRequestByDriverId(req.body.driver_id)
                .then(requests => {
                    for (var request of requests) {
                        events.publishUserRequestUpdated(request);
                    }
                }).catch(err => {
                    console.log(err);
                });
        }).catch((err) => {
            console.log(err);
        });
    
    res.sendStatus(200);
}

var sendUserRequest = function(options, userRequest) {
    var req = http.request(options, function (res) {
        var responseString = "";
    
        res.on("data", function (data) {
            responseString += data;
        });
        res.on("end", function () {
            console.log(responseString); 
        });
    });
    req.write(JSON.stringify(userRequest));
    req.end();
}

/*
var cloneUnlocatedUserRequest = function(userRequest) {
    var unlocatedUserRequest = {
        id: userRequest.id,
        name: userRequest.name,
        phone_number: userRequest.phone_number,
        pickup_address: userRequest.pickup_address,
        formatted_address: userRequest.formatted_address,
        lat: userRequest.lat,
        lng: userRequest.lng,
        note: userRequest.note
    }
    return unlocatedUserRequest;
}
*/