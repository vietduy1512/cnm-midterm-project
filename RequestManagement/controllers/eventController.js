var eventEmitter = require('eventemitter3');
var emitter = new eventEmitter();

var subscribeEvent = (req, res, event) => {
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });

    var heartBeat = setInterval(() => {
        res.write('\n');
    }, 15000);

    var handler = data => {
        var json = JSON.stringify(data);
        res.write(`retry: 500\n`);
        res.write(`event: ${event}\n`);
        res.write(`data: ${json}\n`);
        res.write(`\n`);
    }

    emitter.on(event, handler);

    req.on('close', () => {
        clearInterval(heartBeat);
        emitter.removeListener(event, handler);
    });
}

//
// event pub-sub

var USER_REQUEST_ADDED = 'USER_REQUEST_ADDED';
var USER_REQUEST_UPDATED = 'USER_REQUEST_UPDATED';

var subscribeUserRequestAdded = (req, res) => {
    subscribeEvent(req, res, USER_REQUEST_ADDED);
}

var publishUserRequestAdded = userRequestObj => {
    emitter.emit(USER_REQUEST_ADDED, userRequestObj);
}

var subscribeUserRequestUpdated = (req, res) => {
    subscribeEvent(req, res, USER_REQUEST_UPDATED);
}

var publishUserRequestUpdated = userRequestObj => {
    emitter.emit(USER_REQUEST_UPDATED, userRequestObj);
}

module.exports = {
    subscribeUserRequestAdded,
    publishUserRequestAdded,
    subscribeUserRequestUpdated,
    publishUserRequestUpdated
}