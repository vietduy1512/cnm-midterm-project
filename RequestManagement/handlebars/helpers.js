
function hbsHelpers(hbs) {

    hbs.registerHelper('printState', (state) => {
        if (state == null || state.toLowerCase() == "unlocated") {
            return `<td class="w-100 badge badge-secondary">Unlocated</td>`;
        } else if (state.toLowerCase() == "located") {
            return `<td class="w-100 badge badge-success">Located</td>`;
        } else if (state.toLowerCase() == "taken") {
            return `<td class="w-100 badge badge-primary">Taken</td>`;
        } else if (state.toLowerCase() == "moving") {
            return `<td class="w-100 badge badge-info">Moving</td>`;
        } else if (state.toLowerCase() == "finished") {
            return `<td class="w-100 badge badge-warning">Finished</td>`;
        } else {
            return `<td class="w-100 badge badge-danger">Unknown</td>`;
        }
    });
};

module.exports = hbsHelpers;