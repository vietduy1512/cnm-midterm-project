function sendFindDriversRequest()
{
    $.ajax({
        type: 'get',
        url: '/findDriverForAllLocatedRequest',
        success: function () {
        alert("Send request succeeded");
        }
    });
};


$(function () {
    $('.show-distance-btn').on('click', function (e) {
        var tr = $(this).parent().parent();
        var request = {
            lat: parseFloat(tr.find('td')[5].innerHTML),
            lng: parseFloat(tr.find('td')[6].innerHTML)
        } 
        var driver = {
            lat: parseFloat(tr.find('td').eq(9).attr('value')),
            lng: parseFloat(tr.find('td').eq(10).attr('value'))
        }
        calculateAndDisplayRoute(driver, request);
    });
});