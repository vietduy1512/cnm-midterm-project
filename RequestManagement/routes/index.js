var express = require('express');
var router = express.Router();
var indexCtrl = require('../controllers/indexController');
var authenCtrl = require('../controllers/authenController');
var verifyAccessToken = require('../repos/authRepo').verifyAccessToken;
 
router.get('/',function(req, res) {
    res.redirect('login');
});


router.get('/login', authenCtrl.login_get);
router.post('/login', authenCtrl.login_post);
router.get('/register', authenCtrl.register_get);
router.post('/register', authenCtrl.register_post);

router.get('/main', verifyAccessToken, indexCtrl.main);
router.post('/receiveNewRequestFromLocationIdentifier', indexCtrl.receiveNewRequestFromLocationIdentifier);
router.post('/receiveLocatedRequestFromLocationIdentifier', indexCtrl.receiveLocatedRequestFromLocationIdentifier);

router.post('/receiveTakenRequestFromDriver', indexCtrl.receiveTakenRequestFromDriver);
router.post('/updateMovingState', indexCtrl.updateMovingState);
router.post('/updateFinishState', indexCtrl.updateFinishState);
router.get('/findDriverForAllLocatedRequest', verifyAccessToken, indexCtrl.findDriverForAllLocatedRequest);


module.exports = router;
