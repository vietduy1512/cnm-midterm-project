
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('user_requests').del()
    .then(function () {
      // Inserts seed entries
      return knex('user_requests').insert([
        {id: 1, name: 'Thao', phone_number: '093232232', pickup_address: 'Khoa Học Tự Nhiên Q5', formatted_address: '227 Đường Nguyễn Văn Cừ, Phường 4, Quận 5, Hồ Chí Minh, Vietnam', lat: 10.7624165, lng: 106.6812013, note: '', state: 'Located'},
        {id: 2, name: 'Duy', phone_number: '0938693320', pickup_address: '518/32 Pham Van Chieu P.16', note: 'càng nhanh càng tốt', state: 'Unlocated'},
        {id: 3, name: 'An', phone_number: '090', pickup_address: 'Khoa Học Tự Nhiên Q5', note: '', state: 'Unlocated'},
      ]);
    });
};
