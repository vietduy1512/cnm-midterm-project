
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('user_requests').del()
    .then(function () {
      // Inserts seed entries
      return knex('user_requests').insert([
        {id: 2, name: 'Duy', phone_number: '0938693320', pickup_address: '518/32 Pham Van Chieu P.16', formatted_address: '518/32 Phạm Văn Chiêu, Phường 16, Gò Vấp, Hồ Chí Minh, Vietnam', lat: 10.8512869, lng: 106.659904, note: 'càng nhanh càng tốt'},
        {id: 3, name: 'An', phone_number: '090', pickup_address: 'Khoa Học Tự Nhiên Q5', formatted_address: '227 Đường Nguyễn Văn Cừ, Phường 4, Quận 5, Hồ Chí Minh, Vietnam', lat: 10.7624165, lng: 106.6812013, note: ''}
      ]);
    });
};
