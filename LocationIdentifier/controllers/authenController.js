var authRepo = require('../repos/authRepo');
var userRepo = require('../repos/userRepo');

exports.register_get = function (req, res) {
	res.render('register', { layout: 'authLayout' });
}

exports.register_post = function (req, res) {
	var entity = {
		username: req.body.username,
		name: req.body.name,
		email: req.body.email,
		password: req.body.password,
		permission: 0
	}

	userRepo.add(entity)
		.then(value => {
			res.redirect('login');
		})
		.catch(err => {
			console.log(err);
			res.statusCode = 500;
			res.end('View error log on console');
		})
}

exports.login_get = function (req, res) {
	res.render('login', { layout: 'authLayout' });
}

exports.login_post = function (req, res) {
	var entity = {
		username: req.body.username,
		password: req.body.password
	}

	userRepo.login(entity)
		.then(rows => {
			if (rows.length > 0) {
				var userEntity = rows[0];
				var acToken = authRepo.generateAccessToken(userEntity);
				var rfToken = authRepo.generateRefreshToken();

				authRepo.updateToken(userEntity.id, acToken, rfToken, res)
					.then(value => {
						res.redirect("main");
					})
					.catch(err => {
						console.log(err);
						res.statusCode = 500;
						res.end('View error log on console');
					})
			} else {
				res.json({
					auth: false
				})
			}
		})
		.catch(err => {
			console.log(err);
			res.statusCode = 500;
			res.end('View error log on console');
		})
}