var events = require('./eventController');
var requestify = require('requestify');
var userRequestRepo = require('../repos/user-request-repo');
var http = require("http");

exports.main = function (req, res) {
    var template = '{{#each this}}<tr>' + 
                '<td style="display:none;" value="{{id}}"></td>' +
                '<td>{{name}}</td><td>{{phone_number}}</td><td>{{pickup_address}}</td><td>{{formatted_address}}</td><td>{{lat}}</td><td>{{lng}}</td><td>{{note}}</td>' +
                '<td><form><input type="hidden" name="id" value="{{id}}"><input type="submit" value="Send"></form></td>' +
                '</tr>{{/each}}';

    userRequestRepo.loadAll()
        .then(rows => {
            res.render('main', { template: template, data_table: rows });
        }).catch(err => {
            res.statusCode = 500;
            res.end('View error log on console');
        });
    
}

exports.receiveRequest = function (req, res) {
    var userRequest = {
        name: req.body.name,
        phone_number: req.body.phone_number,
        pickup_address: req.body.pickup_address,
        note: req.body.note
    }

    var options = {
        host: 'localhost',
        port: 3002,
        path: '/receiveNewRequestFromLocationIdentifier',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    requestify.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${req.body.pickup_address}&key=AIzaSyBqcJnhwjKGFn7YN46QwrbR8pLkj_dHKQU`).then(function(response) {
        var locations = response.getBody().results;
        if (typeof(locations[0]) !== undefined) {
            userRequest.formatted_address = locations[0].formatted_address
            userRequest.lat = locations[0].geometry.location.lat
            userRequest.lng = locations[0].geometry.location.lng
        }   
        else {
            userRequest.formatted_address = null;
            userRequest.lat = null;
            userRequest.lng = null;
        }
        var query = `insert into user_requests (name, phone_number, pickup_address, formatted_address, lat, lng, note) values ('${userRequest.name}', '${userRequest.phone_number}', '${userRequest.pickup_address}', '${userRequest.formatted_address}', '${userRequest.lat}', '${userRequest.lng}', '${userRequest.note}')`;
        userRequestRepo.exec(query)
            .then(() => {
                var getQuery = `select id from user_requests where name = '${userRequest.name}' and phone_number = '${userRequest.phone_number}' and pickup_address = '${userRequest.pickup_address}' and formatted_address = '${userRequest.formatted_address}' and lat = '${userRequest.lat}' and lng = '${userRequest.lng}' and note = '${userRequest.note}'`;
                userRequestRepo.exec(getQuery)
                    .then(rows => {
                        if (rows[0] !== undefined) {
                            userRequest.id = rows[0].id;
                            events.publishUserRequestAdded(userRequest);
                            sendUserRequest(options, userRequest);
                        };
                    });
            });
    });

    res.sendStatus(200);
}

exports.updateLocation = function (req, res) {
    userRequestRepo.updateRequestLocationById(req.body.id, req.body.lat, req.body.lng, req.body.formatted_address);
    res.sendStatus(200);
}

exports.sendLocatedRequestToRequestManagement = function (req, res) {
    var userId = parseInt(req.body.id);

    var options = {
        host: 'localhost',
        port: 3002,
        path: '/receiveLocatedRequestFromLocationIdentifier',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    userRequestRepo.getRequestById(userId)
        .then(rows => {
            if (rows[0] !== undefined) {
                var userRequest = {
                    id: rows[0].id,
                    name: rows[0].name,
                    phone_number: rows[0].phone_number,
                    pickup_address: rows[0].pickup_address,
                    formatted_address: rows[0].formatted_address,
                    lat: rows[0].lat,
                    lng: rows[0].lng,
                    note: rows[0].note
                }
                userRequestRepo.removeRequestById(rows[0].id);
                
                sendUserRequest(options, userRequest);
            };
        });

    res.sendStatus(200);
}

var sendUserRequest = function(options, userRequest) {
    var req = http.request(options, function (res) {
        var responseString = "";
    
        res.on("data", function (data) {
            responseString += data;
        });
        res.on("end", function () {
            console.log(responseString); 
        });
    });
    req.write(JSON.stringify(userRequest));
    req.end();
}

/*
var cloneUnlocatedUserRequest = function(userRequest) {
    var unlocatedUserRequest = {
        id: userRequest.id,
        name: userRequest.name,
        phone_number: userRequest.phone_number,
        pickup_address: userRequest.pickup_address,
        formatted_address: userRequest.formatted_address,
        lat: userRequest.lat,
        lng: userRequest.lng,
        note: userRequest.note
    }
    return unlocatedUserRequest;
}
*/