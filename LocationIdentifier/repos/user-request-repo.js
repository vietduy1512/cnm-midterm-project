var db = require('../database/sqlite-db');

exports.loadAll = () => {
	var sql = 'select * from user_requests';
	return db.load(sql);
}

exports.removeRequestById = (id) => {
	var sql = `delete from user_requests where id = ${id}`;
	return db.load(sql);
}

exports.getRequestById = (id) => {
	var sql = `select * from user_requests where id = ${id}`;
	return db.load(sql);
}

exports.updateRequestLocationById = (id, lat, lng, formatted_address) => {
	var sql = `update user_requests set lat='${lat}', lng='${lng}', formatted_address='${formatted_address}' where id = ${id}`;
    return db.load(sql);
}

exports.exec = (sql) => {
	return db.load(sql);
}