var md5 = require('crypto-js/md5');
var db = require('../database/sqlite-db');

exports.loadAll = () => {
	var sql = 'select * from users';
	return db.load(sql);
}

exports.removeuserById = (id) => {
	var sql = `delete from users where id = ${id}`;
	return db.load(sql);
}

exports.getuserById = (id) => {
	var sql = `select * from users where id = ${id}`;
	return db.load(sql);
}

exports.add = userEntity => {
	// userEntity = {
    //     Username: 1,
    //     Password: 'raw pwd',
    //     Name: 'name',
    //     Email: 'email',
    //     Permission: 0
	// }
	
    var md5_pwd = md5(userEntity.password);
    var sql = `insert into users (username, password, name, email, permission) values('${userEntity.username}', '${md5_pwd}', '${userEntity.name}', '${userEntity.email}', ${userEntity.permission})`;

    return db.load(sql);
}

exports.login = loginEntity => {
	// loginEntity = {
	// 	username: 'duylev',
	// 	password: '******'
	// }

    var md5_pwd = md5(loginEntity.password);
	var sql = `select * from users where username = '${loginEntity.username}' and password = '${md5_pwd}'`;
	return db.load(sql);
}

exports.exec = (sql) => {
	return db.load(sql);
}
