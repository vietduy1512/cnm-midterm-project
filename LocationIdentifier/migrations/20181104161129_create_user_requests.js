
exports.up = function(knex, Promise) {
    return knex.schema.createTable('user_requests', function(t) {
        t.increments('id').primary()
        t.string('name').notNullable()
        t.string('phone_number').notNullable()
        t.string('pickup_address').notNullable()
        t.string('formatted_address')
        t.float('lat')
        t.float('lng')
        t.string('note')
        t.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists('user_requests')
};