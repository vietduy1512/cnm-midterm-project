
exports.up = function(knex, Promise) {
    return knex.schema.createTable('readyDrivers', function(t) {
        t.increments('driver_id').primary()
        t.string('socket_id')
        t.float('lat')
        t.float('lng')
        t.integer('stage')
        t.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists('readyDrivers')
};
