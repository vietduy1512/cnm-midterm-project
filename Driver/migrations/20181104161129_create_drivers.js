
exports.up = function(knex, Promise) {
    return knex.schema.createTable('drivers', function(t) {
        t.increments('id').primary()
        t.string('username').notNullable()
        t.string('password').notNullable()
        t.string('name').notNullable()
        t.string('email').notNullable()
        t.integer('permission').notNullable()
        t.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists('drivers')
};