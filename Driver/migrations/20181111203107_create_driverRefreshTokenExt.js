
exports.up = function(knex, Promise) {
    return knex.schema.createTable('driverRefreshTokenExt', function(t) {
        t.increments('driver_id').primary()
        t.string('refresh_token').notNullable()
        t.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists('driverRefreshTokenExt')
};
