var http = require('http');
var driverRepo = require('../repos/driverRepo');
var sessionRepo = require('../repos/sessionRepo');
var requestify = require('requestify');
var io = require('./socketController');

exports.dashboard = function (req, res) {
    res.render('dashboard');
}

exports.map = function (req, res) {
    var driverId = req.cookies['d-driver-id'];
    sessionRepo.getSessionByDriverId(driverId)
        .then(rows => {
            if (rows[0]) {
                res.render('map', { stage: rows[0].stage });
            } else {
                res.render('map');
            }
        });
}

exports.findDriverForRequest = function (req, res) {
    var request = req.body;
    sessionRepo.getfreeDrivers()
        .then(drivers => {
            // Compare Distances
            var apiRequest = generateApiRequest(drivers, request.lat, request.lng);

            requestify.get(apiRequest).then(function(response) {
                var distances = response.getBody();
                var i = 0;
                for (var driver of drivers) {
                    driver.distance = distances.rows[0].elements[i].distance.value;
                    driver.duration = distances.rows[0].elements[i].duration.value;
                    i++;
                }
                sortAscendingDistances(drivers)

                // Send request
                var turn = 0;
                requestDriverAcceptance(drivers, request, turn);

                /*
                socket.emit('chat', function_accept(), function_decline());
                */
            })
        })

    res.sendStatus(200);
}

function requestDriverAcceptance(drivers, request, turn) {
    if (drivers.length == turn) {   // if already check all driver
        return;
    } else {
        var driver = drivers[turn];
        io.sockets.connected[driver.socket_id].emit('requestAcceptance', request, function(isAccept) {
            if (isAccept) {
                acceptPickupRequest(request, driver);
            } else {
                requestDriverAcceptance(drivers, request, turn + 1);
            }
        });
    }
}

function acceptPickupRequest(userRequest, driver) {
    var options = {
        host: 'localhost',
        port: 3002,
        path: '/receiveTakenRequestFromDriver',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    sessionRepo.taken(driver.driver_id);
    driverRepo.getDriverById(driver.driver_id)
        .then(rows => {
            userRequest.driver_id = rows[0].id;
            userRequest.driver_name = rows[0].name;
            userRequest.driver_lat = driver.lat;
            userRequest.driver_lng = driver.lng;
        
            io.sockets.connected[driver.socket_id].emit('pickup', userRequest.lat, userRequest.lng);
        
            sendUserRequest(options, userRequest);
        });
}



function generateApiRequest(drivers, originLat, originLng) {
    var API_KEY = '&key=AIzaSyBqcJnhwjKGFn7YN46QwrbR8pLkj_dHKQU';
    var apiRequest = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${originLat},${originLng}&destinations=`;
    var isSplit = false;
    for (var driver of drivers) {
        if (isSplit) {
            apiRequest += '|';
        } else {
            isSplit = true;
        }
        apiRequest += `${driver.lat},${driver.lng}`;
    }
    apiRequest += API_KEY;
    return apiRequest;
}

function sortAscendingDistances(drivers) {
    for (var i = 0; i < drivers.length - 1; i++) {
        for (var j = i + 1; j < drivers.length; j++) {
            if (drivers[i].distance > drivers[j].distance) {
                var temp = drivers[i];
                drivers[i] = drivers[j];
                drivers[j] = temp;
            }
        }
    }
}


var sendUserRequest = function(options, userRequest) {
    var req = http.request(options, function (res) {
        var responseString = "";
    
        res.on("data", function (data) {
            responseString += data;
        });
        res.on("end", function () {
            console.log(responseString); 
        });
    });
    req.write(JSON.stringify(userRequest));
    req.end();
}