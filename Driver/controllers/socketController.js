var http = require('http');
var socketIO = require('socket.io');
var sessionRepo = require('../repos/sessionRepo');
var app = require('../app');

var server = http.Server(app);
var io = socketIO(server);

io.on('connection', socket => {
    let driverId = socket.handshake.query.driverId;
    sessionRepo.updateSocketId(driverId, socket.id);
    console.log('A driver is connected');

    socket.on('disconnect', () => {
        /*
        sessionRepo.removeBySocketId(socket.id)
            .then(() => {
                console.log('A driver is disconnected');
            });
            */
    });

    socket.on('ready', (driverId, lat, lng) => {
        sessionRepo.ready(driverId, socket.id)
            .then(() => {
                sessionRepo.update_position(driverId, lat, lng)
                    .then(() => {
                        console.log(`Driver is ready with socketId: ${socket.id}, lat:${lat}, lng:${lng}`);
                        io.to(`${socket.id}`).emit('readyEnabled');
                    });
            });
    });

    socket.on('standby', driverId => {
        sessionRepo.standb(driverId)
            .then(() => {
                console.log(`Driver is standby with socketId: ${socket.id}`);
                io.to(`${socket.id}`).emit('standbyEnabled');
            });
    });

    socket.on('update_position', (driverId, lat, lng) => {
        sessionRepo.update_position(driverId, lat, lng)
            .then(() => {
                console.log(`Driver's position is updated: ${socket.id}, lat:${lat}, lng:${lng}`);
            });
    });

    socket.on('moving', driverId => {
        var options = {
            host: 'localhost',
            port: 3002,
            path: '/updateMovingState',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        };
    
        var obj = {
            driver_id: driverId
        }
        sessionRepo.moving(driverId);
        io.to(`${socket.id}`).emit('moving');
        sendUserRequest(options, obj);
    });

    socket.on('finish', driverId => {
        var options = {
            host: 'localhost',
            port: 3002,
            path: '/updateFinishState',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        };
    
        sessionRepo.free(driverId);
        var obj = {
            driver_id: driverId
        }
        io.to(`${socket.id}`).emit('finish');
        sendUserRequest(options, obj);
    });
});

const PORT = process.env.PORT || 3004;

server.listen(PORT, () => {
    console.log(`listening on *:${PORT}`);
});

var sendUserRequest = function(options, userRequest) {
    var req = http.request(options, function (res) {
        var responseString = "";
    
        res.on("data", function (data) {
            responseString += data;
        });
        res.on("end", function () {
            console.log(responseString); 
        });
    });
    req.write(JSON.stringify(userRequest));
    req.end();
}

module.exports = io;