var jwt = require('jsonwebtoken');
var rndToken = require('rand-token');
var moment = require('moment');
var driverRepo = require('../repos/driverRepo');

var db = require('../database/sqlite-db');

const SECRET = 'VIETDUY';
const AC_LIFETIME = 600; // seconds

exports.generateAccessToken = driverEntity => {
      // TODO: remove password from acToken
    var payload = {
        driver: driverEntity,
        info: 'more info'
    }

    var token = jwt.sign(payload, SECRET, {
        expiresIn: AC_LIFETIME
    });

    return token;
}

exports.verifyAccessToken = (req, res, next) => {
    var acToken = req.cookies['d-access-token'];
    var rfToken = req.cookies['d-refresh-token'];

    if (acToken) {
        jwt.verify(acToken, SECRET, (err, payload) => {
            if (err) {
                if (err.name == "TokenExpiredError") {

                    var sql = `select driver_id from driverRefreshTokenExt where refresh_token = '${rfToken}'`;
                    db.load(sql)
                        .then(value => {
                            if (value[0]) {
                                driverRepo.getDriverById(value[0].driver_id)
                                    .then(driver => {
                                        if (driver[0]) {
                                            var newAcToken = this.generateAccessToken(driver[0]); 
                                            var newRfToken = this.generateRefreshToken();
                                            this.updateToken(driver[0].id, newAcToken, newRfToken, res)
                                                .then(value => {
                                                    next();
                                                });
                                        };
                                    });
                            } else {
                                res.statusCode = 401;
                                res.json({
                                    msg: 'INVALID REFRESH TOKEN',
                                    error: err
                                })
                            }
                        });
                } else {
                    res.statusCode = 401;
                    res.json({
                        msg: 'INVALID ACCESS TOKEN',
                        error: err
                    })
                }
            } else {
                req.token_payload = payload;
                next();
            }
        });
    } else {
        res.statusCode = 403;
        res.json({
            msg: 'NO_TOKEN'
        })
    }
}

exports.generateRefreshToken = () => {
    const SIZE = 80;
    return rndToken.generate(SIZE);
}

exports.updateToken = (driverId, acToken, rfToken, res) => {
    res.cookie('d-driver-id', driverId);
    res.cookie('d-access-token', acToken);
    res.cookie('d-refresh-token', rfToken);

    return new Promise((resolve, reject) => {

        var sql = `delete from driverRefreshTokenExt where driver_id = ${driverId}`;
        db.load(sql) // delete
            .then(value => {
                sql = `insert into driverRefreshTokenExt (driver_id, refresh_token) values(${driverId}, '${rfToken}')`;
                return db.load(sql);
            })
            .then(value => resolve(value))
            .catch(err => reject(err));
    });
}