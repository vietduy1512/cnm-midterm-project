var md5 = require('crypto-js/md5');
var db = require('../database/sqlite-db');

exports.loadAll = () => {
	var sql = 'select * from drivers';
	return db.load(sql);
}

exports.removeDriverById = (id) => {
	var sql = `delete from drivers where id = ${id}`;
	return db.load(sql);
}

exports.getDriverById = (id) => {
	var sql = `select * from drivers where id = ${id}`;
	return db.load(sql);
}

exports.add = driverEntity => {
	// driverEntity = {
    //     Username: 1,
    //     Password: 'raw pwd',
    //     Name: 'name',
    //     Email: 'email',
    //     Permission: 0
	// }
	
    var md5_pwd = md5(driverEntity.password);
    var sql = `insert into drivers (username, password, name, email, permission) values('${driverEntity.username}', '${md5_pwd}', '${driverEntity.name}', '${driverEntity.email}', ${driverEntity.permission})`;

    return db.load(sql);
}

exports.login = loginEntity => {
	// loginEntity = {
	// 	username: 'duylev',
	// 	password: '******'
	// }

    var md5_pwd = md5(loginEntity.password);
	var sql = `select * from drivers where username = '${loginEntity.username}' and password = '${md5_pwd}'`;
	return db.load(sql);
}

exports.exec = (sql) => {
	return db.load(sql);
}
