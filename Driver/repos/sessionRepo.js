var db = require('../database/sqlite-db');

exports.getSessionByDriverId = (driverId) => {
    var sql = `select * from readyDrivers where driver_id = ${driverId}`;
    return db.load(sql);
}

exports.ready = (driverId, socketId) => {
    //delete from readyDrivers where driver_id = ${driverId};
    var sql = `insert into readyDrivers (driver_id, socket_id, stage) values (${driverId}, '${socketId}', 0)`;
    return db.load(sql);
}

exports.standb = (driverId) => {
    var sql = `delete from readyDrivers where driver_id = ${driverId}`;
    return db.load(sql);
}

exports.removeBySocketId = (socketId) => {
    var sql = `delete from readyDrivers where socket_id = '${socketId}' and stage=0`;
    return db.load(sql);
}

exports.updateSocketId = (driverId, socketId) => {
    var sql = `update readyDrivers set socket_id='${socketId}' where driver_id = ${driverId}`;
    return db.load(sql);
}

exports.free = (driverId) => {
    var sql = `update readyDrivers set stage=0 where driver_id = ${driverId}`;
    return db.load(sql);
}

exports.taken = (driverId) => {
    var sql = `update readyDrivers set stage=1 where driver_id = ${driverId}`;
    return db.load(sql);
}

exports.moving = (driverId) => {
    var sql = `update readyDrivers set stage=2 where driver_id = ${driverId}`;
    return db.load(sql);
}

exports.getfreeDrivers = () => {
    var sql = `select * from readyDrivers where stage = 0`;
    return db.load(sql);
}

exports.update_position = (driverId, lat, lng) => {
    var sql = `update readyDrivers set lat='${lat}', lng='${lng}' where driver_id = ${driverId}`;
    return db.load(sql);
}

exports.clean = () => {
    var sql = `delete from readyDrivers`;
    return db.load(sql);
}