
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('drivers').del()
    .then(function () {
      // Inserts seed entries
      return knex('drivers').insert([
        {id: 1, username: 'duylev', password: '8f88d5542f6e43f954f07a3c192913b3', name: 'Lê Viết Duy', email: 'vietduy1512@gmail.com', permission: 1},
        {id: 2, username: 'thienan', password: '287cb3073f7bd8d326810d07165a55a3', name: 'An An', email: 'testing@gmail.com', permission: 1},
      ]);
    });
};
