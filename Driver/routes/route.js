var express = require('express');
var router = express.Router();
var authenCtrl = require('../controllers/authenController');
var driverCtrl = require('../controllers/driverController');
var verifyAccessToken = require('../repos/authRepo').verifyAccessToken;

router.get('', function(req, res) {
    res.redirect('login');
})
router.get('/login', authenCtrl.login_get);
router.post('/login', authenCtrl.login_post);
router.get('/register', authenCtrl.register_get);
router.post('/register', authenCtrl.register_post);

//router.get('/dashboard', verifyAccessToken, driverCtrl.dashboard);
router.get('/map', verifyAccessToken, driverCtrl.map);


// process between Driver and RequestManagement
router.post('/findDriverForRequest', driverCtrl.findDriverForRequest);



module.exports = router;
