window.onload = function() {
    init();
};

var socket = io(`http://localhost:3004?driverId=${$.cookie("d-driver-id")}`);
var _currentRequest;
var _callbackFunc;
var acceptCountdown;

var init = function() {
    updateDriverPosition();
    initStage();
    initSocket();
    initEvent();
};

function initStage() {
    var stage = $('#driver-stage').attr('value');
    switch(stage) {
        case "":
            $('#readyCheckBox').prop('checked', false);
            break;
        case "0":
            $('#readyCheckBox').prop('checked', true);
            break;
        case "1":
            $('#readyCheckBox').prop('checked', true);
            getDriverPosition().then(latlng => {
                var userPosition = {
                  lat: parseFloat($.cookie("request_lat")),
                  lng: parseFloat($.cookie("request_lng"))
                };
                calculateAndDisplayRoute(latlng, userPosition);
                alert("Click Start after pickup Guest");
                $("#d-start-btn").show();
            });
            break;
        case "2":
            $('#readyCheckBox').prop('checked', true);
            alert("Click Finish when reaching destination");
            $("#d-start-btn").hide();
            $("#d-finish-btn").show();
            break;
        default:
    }
}

function initSocket() {
    socket.on('readyEnabled', function() {
        alert("You are ready to accept request!")
    });

    socket.on('standbyEnabled', function() {
        alert("You are standby.")
    });

    socket.on('requestAcceptance', function(request, callbackFunc) {
        _currentRequest = request;
        _callbackFunc = callbackFunc;

        // Count down
        var time = 11000;
        // Update the count down every 1 second
        acceptCountdown = setInterval(function() {
            time -= 1000;
            var seconds = Math.floor((time % (1000 * 60)) / 1000);
            $('.modal-body').html(`Countdown: ${seconds}s<br/><strong>User Address:</strong> ${request.pickup_address}<br/><strong>Located Address:</strong> ${request.formatted_address}`)
            
            // If the count down is over
            if (time < 0) {
                _callbackFunc(false);
                $('#d-modal').modal('hide');
                clearInterval(acceptCountdown);
                acceptCountdown = null;
            }
        }, 1000);
        $('.modal-body').html(`Countdown: 10s<br/><strong>User Address:</strong> ${request.pickup_address}<br/><strong>Located Address:</strong> ${request.formatted_address}`)
        $('#d-modal').modal('show');
    });

    socket.on('pickup', function(lat, lng) {
        getDriverPosition().then(latlng => {
            var userPosition = {
              lat: lat,
              lng: lng
            };
            $.cookie("request_lat", lat);
            $.cookie("request_lng", lng);
            calculateAndDisplayRoute(latlng, userPosition);
            alert("Click Start after pickup Guest");
            $("#d-start-btn").show();
        });
    });

    socket.on('moving', function() {
        alert("Click Finish when reaching destination");
        $("#d-start-btn").hide();
        $("#d-finish-btn").show();
    });

    socket.on('finish', function() {
        alert("Finish successfully");
        $("#d-finish-btn").hide();
    });
};

function initEvent() {
    $(function() {
		$("#d-accept-btn").click(function () {
            _callbackFunc(true);
        });
        
		$("#d-decline-btn").click(function () {
            _callbackFunc(false);
            if (acceptCountdown) {
                clearInterval(acceptCountdown);
                acceptCountdown = null;
            }
        });
        
        $("#d-start-btn").click(function () {
            var driverId = parseInt(getCookie('d-driver-id'));
            stopDisplayRoute();
            socket.emit('moving', driverId);
        });
        
		$("#d-finish-btn").click(function () {
            var driverId = parseInt(getCookie('d-driver-id'));
            socket.emit('finish', driverId);
        });
	});
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function updateDriverPosition() {
    getDriverPosition().then(myLatLng => {
        addMarker(myLatLng);
    });
}

function getDriverPosition() {
    return new Promise((resolve, reject) => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                var latlng = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                }
                resolve(latlng);
            });
        } else { 
            reject("Geolocation is not supported by this browser.");
        };
    });  
}

var rad = function(x) {
    return x * Math.PI / 180;
};
  
var getHaversineDistance = function(p1, p2) {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2.lat - p1.lat);
    var dLong = rad(p2.lng - p1.lng);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
};

// Using socketio
$(document).ready(function() {
    $('#readyCheckBox').change(function() {
        if($(this).is(":checked")) {
            var driverId = parseInt(getCookie('d-driver-id'));
            getDriverPosition().then(myLatLng => {
                socket.emit('ready', driverId, myLatLng.lat, myLatLng.lng);
            });
        } else {
            socket.emit('standby', 1); // id
        };
    });
});


