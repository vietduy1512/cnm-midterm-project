var map;
var marker;
var directionsService;
var directionsDisplay;

function initMap() {
  var myLatLng = {lat: 10.79155, lng: 106.6724};
  map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 14,
      mapTypeId: 'roadmap'
  });

  marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    animation: google.maps.Animation.DROP,
  });
  marker.setMap(null);

  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  map.addListener('click', function(e) {
    updateDriverPosition();
    getDriverPosition().then(latlng => {
      var clickPosition = {
        lat: e.latLng.lat(),
        lng: e.latLng.lng()
      };
      var driverId = parseInt(getCookie('d-driver-id'));
      socket.emit('update_position', driverId, latlng.lat, latlng.lng);
  
      var distance = getHaversineDistance(clickPosition, latlng);
      if(distance > 100) {
        alert('Error! Distance is over 100m');
      } 
    });
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}

function addMarker(myLatLng) {
  marker.setMap(null);
  marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    animation: google.maps.Animation.DROP,
    title: 'User'
  });

  map.panTo(myLatLng);

  var contentString = '<div>Current position</div>'

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
    toggleBounce();
  });

  addMarkerEvent(marker);
}

function addMarkerEvent(marker){
  google.maps.event.addListener(marker, 'dragstart', function(e) {
  
  });

  google.maps.event.addListener(marker, 'dragend', function(e) {
    var latLng = {
      lat: e.latLng.lat(),
      lng: e.latLng.lng()
  } 
    console.log(latLng)
   //$(".selected").blabla
  });

  google.maps.event.addListener(marker, 'drag', function(e) {
  
  });
}

function toggleBounce() {
  if (marker.getAnimation() !== null) {
      marker.setAnimation(null);
  } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}


function calculateAndDisplayRoute(start, end) {
  directionsDisplay.setMap(map);
    directionsService.route({
      origin: start,
      destination: end,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
}

function stopDisplayRoute() {
  directionsDisplay.setMap(null);
}