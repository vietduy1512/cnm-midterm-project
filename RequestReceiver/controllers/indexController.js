var http = require("http");

exports.form = function (req, res) {
    res.render('form');
}

exports.sendRequest = function (req, res) {
    var userRequest = {
        name: req.body.name,
        phone_number: req.body.phone_number,
        pickup_address: req.body.pickup_address,
        note: req.body.note
    }

    var options = {
        host: 'localhost',
        port: 3001,
        path: '/receiveRequest',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      };

    var req = http.request(options, function (res) {
        var responseString = "";
    
        res.on("data", function (data) {
            responseString += data;
        });
        res.on("end", function () {
            console.log(responseString); 
        });
    });
    req.write(JSON.stringify(userRequest));
    req.end();

    res.sendStatus(200);
}