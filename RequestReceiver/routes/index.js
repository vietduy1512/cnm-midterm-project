var express = require('express');
var router = express.Router();
var authenCtrl = require('../controllers/authenController');
var indexCtrl = require('../controllers/indexController');
var verifyAccessToken = require('../repos/authRepo').verifyAccessToken;

router.get('/',function(req, res) {
    res.redirect('login');
});

router.get('/login', authenCtrl.login_get);
router.post('/login', authenCtrl.login_post);
router.get('/register', authenCtrl.register_get);
router.post('/register', authenCtrl.register_post);

router.get('/form', verifyAccessToken, indexCtrl.form);
router.post('/sendRequest', verifyAccessToken, indexCtrl.sendRequest);

module.exports = router;
