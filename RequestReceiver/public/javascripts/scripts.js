$(function () {
    $('form').on('submit', function (e) {
        var form = $(this);
        $.ajax({
            type: 'post',
            url: '/sendRequest',
            data: $(this).serialize(),
            success: function () {
                form[0].reset();
                alert("Send request successfully");
            }
        });
        e.preventDefault();
    });
});